const logins=require("./2-arrays-logins.cjs");

const splitIp=(logins) => logins.map((user) => {
  return {
      ...user,
      ip_address:  user.ip_address.split('.').map((item) =>+item)
  }
});
console.log(splitIp(logins));